from .abstractsortclass import *


class BubbleSort(SortingBaseClass):

    def animate(self, yield_frame, plot_bars: container.BarContainer):
        self.iteration[0] += 1

        index_1, index_2 = yield_frame[1]
        for i, (bar, value) in enumerate(zip(plot_bars, yield_frame[0])):
            bar.set_height(value)
            if i == index_1 or i == index_2:
                bar.set_color('orange')
            else:
                bar.set_color('blue')

        self.text.set_text("iterations : {}".format(self.iteration[0]))

    def sort(self, arr, bars: container.BarContainer, *arg):
        for i in range(len(arr)-1):
            for j in range(len(arr)-1-i):
                if arr[j] > arr[j+1]:
                    arr[j], arr[j+1] = arr[j+1], arr[j]
                    bars.datavalues = arr
                    yield arr, (j, j+1)
