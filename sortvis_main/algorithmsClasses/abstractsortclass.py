from abc import ABC, abstractmethod
import random
import matplotlib.container as container
import matplotlib.pyplot as plt


class SortingBaseClass(ABC):
    # fixed sample size - for Testing only
    # data = ([10, 80, 30, 90, 40, 50, 70])

    iteration = [0]
    data = [random.randint(1, 100) for _ in range(50)]
    fig, ax = plt.subplots(figsize=(8, 5))
    plot_bars: container.BarContainer = ax.bar(range(len(data)), data)
    text = ax.text(0.01, 0.95, "", transform=ax.transAxes)

    @abstractmethod
    def animate(self):
        pass

    @abstractmethod
    def sort(self):
        pass
