from .abstractsortclass import *


class QuickSort(SortingBaseClass):

    def animate(self, yield_frame, plot_bars: container.BarContainer):
        self.iteration[0] += 1

        index_1, index_2 = yield_frame[1]
        for i, (bar, value) in enumerate(zip(plot_bars, yield_frame[0])):
            bar.set_height(value)
            if i == index_1 or i == index_2:
                bar.set_color('orange')
            else:
                bar.set_color('blue')

        self.text.set_text("iterations : {}".format(self.iteration[0]))

    def sort(self, arr, bars: container.BarContainer, *arg):
        low = 0
        high = len(arr)-1
        yield from self.quick_sort_impl(arr, bars, low, high)

    def quick_sort_impl(self, arr, bars: container.BarContainer, low, high):
        # Stop condition
        if(low < high):
            # create generators for the pivot
            par = yield from self.partition(arr, bars, low, high)
            # create generators for left of pivot
            yield from self.quick_sort_impl(arr, bars, low, par-1)
            # create generators for right of pivot
            yield from self.quick_sort_impl(arr, bars, par+1, high)

    def partition(self, arr, bars: container.BarContainer, low, high):
        # select elements
        pivot = arr[high]
        i = low-1
        for j in range(low, high):
            # move all elements smaller to the pivot to the right
            if arr[j] < pivot:
                i += 1
                arr[i], arr[j] = arr[j], arr[i]
                # present change
                bars.datavalues = arr
                yield arr, (i, j)
        # swap the last element to the right of the small index (which is i)
        arr[i+1], arr[high] = arr[high], arr[i+1]
        # present change
        bars.datavalues = arr
        yield arr, (i, j)
        # return pivot - this is equal to stoping Iteration and returns pivot
        return i+1
