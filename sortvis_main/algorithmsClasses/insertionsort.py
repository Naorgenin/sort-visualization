from .abstractsortclass import *


class InsertionSort(SortingBaseClass):

    def animate(self, yield_frame, plot_bars: container.BarContainer):
        self.iteration[0] += 1

        index_1, index_2 = yield_frame[1]
        for i, (bar, value) in enumerate(zip(plot_bars, yield_frame[0])):
            bar.set_height(value)
            if i == index_1 or i == index_2:
                bar.set_color('orange')
            else:
                bar.set_color('blue')

        self.text.set_text("iterations : {}".format(self.iteration[0]))

    def sort(self, arr, bars: container.BarContainer, *arg):
        for i in range(1, len(arr)):
            key = arr[i]

            prev = i-1
            # check the previous key with the current key
            # push it back if its smaller
            while prev >= 0 and key < arr[prev]:
                # move element ahead if its bigger than the key
                arr[prev+1] = arr[prev]
                prev -= 1
                bars.datavalues = arr
                yield arr, (prev, prev+1)

            arr[prev+1] = key  # key is placed in its appropritate index
