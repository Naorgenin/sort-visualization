from .abstractsortclass import *


class MergeSort(SortingBaseClass):
    pass
# TODO understand whats happening lol

    def animate(self, yield_frame, plot_bars: container.BarContainer):
        self.iteration[0] += 1
        index_1, index_2 = yield_frame[1]
        for i, (bar, value) in enumerate(zip(plot_bars, yield_frame[0])):
            bar.set_height(value)
            if i == index_1 or i == index_2:
                bar.set_color('orange')
            else:
                bar.set_color('blue')

        self.text.set_text("iterations : {}".format(self.iteration[0]))

    def sort(self, arr,  bars: container.BarContainer):
        yield from self.merge_sort(arr,  bars, 0, len(arr)-1)

    def merge_sort(self, arr,  bars: container.BarContainer, start, end):
        if end <= start:
            return
        mid = start + ((end - start + 1) // 2) - 1

    # yield from statements have been used to yield
    # the array from the functions
        yield from self.merge_sort(arr, bars, start, mid)
        yield from self.merge_sort(arr, bars, mid + 1, end)
        yield from self.merge(arr, bars, start, mid, end)

    def merge(self, arr,  bars: container.BarContainer, start, mid, end):
        merged = []
        leftIdx = start
        rightIdx = mid + 1

        while leftIdx <= mid and rightIdx <= end:
            if arr[leftIdx] < arr[rightIdx]:
                merged.append(arr[leftIdx])
                leftIdx += 1
            else:
                merged.append(arr[rightIdx])
                rightIdx += 1

        while leftIdx <= mid:
            merged.append(arr[leftIdx])
            leftIdx += 1

        while rightIdx <= end:
            merged.append(arr[rightIdx])
            rightIdx += 1

        for i in range(len(merged)):
            arr[start + i] = merged[i]
            bars.datavalues = arr
            yield arr, (start + i, i)

        # if len(array) > 1:
        #     # divide the array into two halves
        #     mid = len(array) // 2
        #     left_half = array[:mid]
        #     right_half = array[mid:]

        #     # recursively sort the halves
        #     yield from self.sort(left_half, bars, mid)
        #     yield from self.sort(right_half, bars, mid)

        #     # merge the sorted halves
        #     i = j = k = 0
        #     # yield array, (k, i)
        #     while i < len(left_half) and j < len(right_half):
        #         if left_half[i] < right_half[j]:
        #             array[k] = left_half[i]
        #             i += 1
        #             yield array, (k, i, mid)
        #         else:
        #             array[k] = right_half[j]
        #             j += 1
        #             yield array, (k, j, mid)
        #         k += 1
        #         # bars.datavalues = array
        #         # yield array, (i, j)
        #     while i < len(left_half):
        #         array[k] = left_half[i]
        #         yield array, (k, i, mid)
        #         i += 1
        #         k += 1
        #         bars.datavalues = array
        #     while j < len(right_half):
        #         array[k] = right_half[j]
        #         yield array, (k, j, mid)
        #         j += 1
        #         k += 1
        #         bars.datavalues = array


# TODO
# try to look at each stage of the merge,
# take the subframe and the swap indexs(indexs dont look correct), use that to replace the array
# [10, 80, 30, 90, 40, 50, 70]([30, 80], (1, 1))
# [10, 80, 30, 90, 40, 50, 70]([10, 30, 30], (1, 1))
# [10, 80, 30, 90, 40, 50, 70]([10, 30, 80], (1, 2))
# [10, 80, 30, 90, 40, 50, 70]([40, 90], (1, 1))
# [10, 80, 30, 90, 40, 50, 70]([50, 70], (1, 1))
# [10, 80, 30, 90, 40, 50, 70]([40, 50, 70, 90], (2, 2))
# [10, 30, 40, 50, 70, 80, 90]([10, 30, 40, 50, 70, 80, 90], (3, 4))
