
from matplotlib import container
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from algorithmsClasses.bubblesort import BubbleSort
from algorithmsClasses.insertionsort import InsertionSort
from algorithmsClasses.quicksort import QuickSort
from algorithmsClasses.merge_sort import MergeSort

algo_dict: dict = {1: BubbleSort,
                   2: InsertionSort, 3: QuickSort, 4: MergeSort}

print("Welcome to the alogrithm visualizer!")
flag = True
while flag:

    print("Select your Algorithm via number!")
    for k, v in algo_dict.items():
        print(k, " ".join(v.__name__.capitalize().split("_")))
    try:
        algo = input()
        if not isinstance(int(algo), int):
            raise TypeError("Only integers are allowed")
        algo = int(algo)
        if algo in algo_dict.keys():
            flag = False
        else:
            print("Invalid Input, Try again")
    except TypeError as e:
        print(e)
        exit(1)

# create the generated sort
generator = algo_dict[algo]()

#  Set up the animation
# TODO set s1peed based on size
ani = animation.FuncAnimation(generator.fig,
                              func=generator.animate, fargs=([generator.plot_bars]), frames=generator.sort(generator.data, generator.plot_bars), interval=100, repeat=False)
plt.show()

# merge sort requires a diffrent update mechanism, need to remember where i am during the sort
# refactor needed, abstract class/interface algorithm with the algos as sons
# try TDD method, add a new algo post refactor and see it works with the algo without a interface/class

#####################
# TEST GENERATORS
# [10, 80, 30, 90, 40, 50, 70]
# A = MergeSort()
# generator2 = A.sort(A.data, A.plot_bars)
# for i in generator2:
#     print(i)
#####################
