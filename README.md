Gui visualizer for sorting algorithms

Bubble Sort
![Code in Action ](sortvis_main/readme/bubble_sort.gif "bubble sort")

Insertion Sort
![Code in Action ](sortvis_main/readme/insertion_sort.gif "insertion sort")

Quick Sort
![Code in Action ](sortvis_main/readme/quick_sort.gif "quick sort")

Merge Sort
![Code in Action ](sortvis_main/readme/merge_sort.gif "merge sort")